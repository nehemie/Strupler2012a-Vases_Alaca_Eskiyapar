# README

This is the text I submitted for publication to Anatolia Antiqua XX (2012)

## Text

I have reformatted the author's manuscript version to asciidoctor format
(see [Asciidoctor](http://asciidoctor.org)). It is a dummy test to try
something different than markdown. The bibliography is compiled with the gem
[asciidoctor-bibtex](https://github.com/asciidoctor/asciidoctor-bibtex)

To reproduce from source:

    asciidoctor -a data-uri -r asciidoctor-bibtex Strupler2012-RVAE.adoc


## Images

In the directory [images](/images) I provide a vector file (.svg)
whenever possible

## License

Image and text: Néhémie Strupler CC BY-SA 4.0

